package com.hbo.eniso.mvc.dao;

import com.hbo.eniso.mvc.entities.Participant;

public interface IParticipantDao extends IGenericDao<Participant>{

}
