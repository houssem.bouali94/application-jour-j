package com.hbo.eniso.mvc.dao.impl;

import com.hbo.eniso.mvc.dao.IIndustrielDao;
import com.hbo.eniso.mvc.entities.Industriel;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class IndustrielDaoImpl extends GenericDaoImpl<Industriel> implements IIndustrielDao {

    public IndustrielDaoImpl() {
        super(Industriel.class);
    }
}
