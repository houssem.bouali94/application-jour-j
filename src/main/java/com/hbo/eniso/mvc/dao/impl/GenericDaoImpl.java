package com.hbo.eniso.mvc.dao.impl;

import com.hbo.eniso.mvc.dao.IGenericDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.Query;
import java.util.List;

@SuppressWarnings("unchecked")
public class GenericDaoImpl<E> implements IGenericDao<E> {

    private final Class<E> type;
    @Autowired
    private SessionFactory sessionFactory;

    public GenericDaoImpl(Class<E> entityClass) {
//        Type t = getClass().getGenericSuperclass();
//        ParameterizedType pt = (ParameterizedType) t;
//        type = (Class<E>) pt.getActualTypeArguments()[0];
        type = entityClass;
    }

    @Override
    public E save(E entity) {
        Session session = this.sessionFactory.getCurrentSession();
        session.save(entity);
        return entity;
    }

    @Override
    public E update(E entity) {
        Session session = this.sessionFactory.getCurrentSession();
        return (E) session.merge(entity);
    }

    @Override
    public List<E> selectAll() {
        Session session = this.sessionFactory.getCurrentSession();
        Query query = session.createQuery("select t from " + type.getSimpleName() + " t");
        return query.getResultList();
    }

    @Override
    public E getById(Long id) {
        Session session = this.sessionFactory.getCurrentSession();
        return session.find(type, id);
    }

    @Override
    public void remove(Long id) {
        Session session = this.sessionFactory.getCurrentSession();
        E tab = session.getReference(type, id);
        session.remove(tab);
    }

    @Override
    public E findOne(String paramName, Object paramValue) {
        Session session = this.sessionFactory.getCurrentSession();
        Query query = session.createQuery("select t from " + type.getSimpleName() + " t where " + paramName + " = :x ");
        query.setParameter(paramName, paramValue);
        return query.getResultList().size() > 0 ? (E) query.getResultList().get(0) : null;
    }

    public Class<E> getType() {
        return type;
    }

}
