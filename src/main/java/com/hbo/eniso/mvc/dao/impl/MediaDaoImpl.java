package com.hbo.eniso.mvc.dao.impl;

import com.hbo.eniso.mvc.dao.IMediaDao;
import com.hbo.eniso.mvc.entities.Media;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class MediaDaoImpl extends GenericDaoImpl<Media> implements IMediaDao {

    public MediaDaoImpl() {
        super(Media.class);
    }
}
