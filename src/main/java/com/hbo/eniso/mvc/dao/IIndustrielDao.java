package com.hbo.eniso.mvc.dao;

import com.hbo.eniso.mvc.entities.Industriel;

public interface IIndustrielDao extends IGenericDao<Industriel> {

}
