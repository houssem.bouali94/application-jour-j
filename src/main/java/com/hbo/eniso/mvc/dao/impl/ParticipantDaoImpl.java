package com.hbo.eniso.mvc.dao.impl;

import com.hbo.eniso.mvc.dao.IParticipantDao;
import com.hbo.eniso.mvc.entities.Participant;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class ParticipantDaoImpl extends GenericDaoImpl<Participant> implements IParticipantDao {

    public ParticipantDaoImpl() {
        super(Participant.class);
    }
}
