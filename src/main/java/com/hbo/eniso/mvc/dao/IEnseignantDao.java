package com.hbo.eniso.mvc.dao;

import com.hbo.eniso.mvc.entities.Enseignant;

public interface IEnseignantDao extends IGenericDao<Enseignant>{

}
