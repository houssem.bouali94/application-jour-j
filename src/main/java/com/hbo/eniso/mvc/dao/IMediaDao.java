package com.hbo.eniso.mvc.dao;

import com.hbo.eniso.mvc.entities.Media;

public interface IMediaDao extends IGenericDao<Media>{

}
