package com.hbo.eniso.mvc.dao.impl;

import com.hbo.eniso.mvc.dao.IEnseignantDao;
import com.hbo.eniso.mvc.entities.Enseignant;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class EnseignantDaoImpl extends GenericDaoImpl<Enseignant> implements IEnseignantDao {

    public EnseignantDaoImpl() {
        super(Enseignant.class);
    }
}
