package com.hbo.eniso.mvc.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

/**
 * @author Houssem BOUALI
 * @since 18/05/2022
 **/
@Configuration
public class RegisteredBeans {

    /**
     * In order to resolve ${} in {@link org.springframework.beans.factory.annotation.Value},
     * we need to register a <b>static</b> {@link org.springframework.context.support.PropertySourcesPlaceholderConfigurer}
     *
     * @return PropertySourcesPlaceholderConfigurer
     */
    @Bean
    public static PropertySourcesPlaceholderConfigurer getPropertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}
