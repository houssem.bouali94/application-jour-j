package com.hbo.eniso.mvc.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

/**
 * This class is the replacement for <a href="src/main/webapp/WEB-INF/spring/appServlet/servlet-context.xml"> servlet-context.xml</a> <br/>
 *
 * {@link EnableWebMvc} replaces the {@code <annotation-driven />} tag
 *
 * @author Houssem BOUALI
 * @since 18/05/2022
 **/
@EnableWebMvc
@Configuration
@ComponentScan(basePackages = {"com.hbo.eniso.mvc.*"})
public class WebConfig implements WebMvcConfigurer {


    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("index");
    }

    /**
     * Resolves views selected for rendering by @Controllers to .jsp resources in the /WEB-INF/views directory
     * <br/><br/>
     * This method replaces <pre>{@code
     * <beans:bean class="org.springframework.web.servlet.view.InternalResourceViewResolver">
     * 		<beans:property name="prefix" value="/WEB-INF/views/" />
     * 		<beans:property name="suffix" value=".jsp" />
     * 	</beans:bean>}</pre>
     * @return {@link ViewResolver}
     */
    @Bean
    public ViewResolver viewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setViewClass(JstlView.class);
        resolver.setPrefix("/WEB-INF/views/");
        resolver.setSuffix(".jsp");

        return resolver;
    }

    /**
     * Handles HTTP GET requests for /resources/** by efficiently serving up static resources in the ${webappRoot}/resources directory
     * <br/><br/>
     * This method replaces <pre>{@code <resources mapping="..." location="..." />} tag</pre>
     * @param registry ResourceHandlerRegistry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**")
                .addResourceLocations("/resources/");
    }
}
