package com.hbo.eniso.mvc.services.impl;

import com.hbo.eniso.mvc.dao.IParticipantDao;
import com.hbo.eniso.mvc.entities.Participant;
import com.hbo.eniso.mvc.services.IParticipantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ParticipantServiceImpl implements IParticipantService {

    @Autowired
    private IParticipantDao dao;

    @Override
    public Participant save(Participant entity) {
        return dao.save(entity);
    }

    @Override
    public Participant update(Participant entity) {
        return dao.update(entity);
    }

    @Override
    public List<Participant> selectAll() {
        return dao.selectAll();
    }

    @Override
    public Participant getById(Long id) {
        return dao.getById(id);
    }

    @Override
    public void remove(Long id) {
        dao.remove(id);
    }

    @Override
    public Participant findOne(String paramName, Object paramValue) {
        return dao.findOne(paramName, paramValue);
    }

}
