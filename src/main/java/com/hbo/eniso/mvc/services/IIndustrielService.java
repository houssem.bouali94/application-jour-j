package com.hbo.eniso.mvc.services;

import java.util.List;

import com.hbo.eniso.mvc.entities.Industriel;

public interface IIndustrielService {

	public Industriel save(Industriel entity);

	public Industriel update(Industriel entity);

	public List<Industriel> selectAll();

	public Industriel getById(Long id);

	public void remove(Long id);

	public Industriel findOne(String paramName, Object paramValue);
}
