package com.hbo.eniso.mvc.services.impl;

import java.util.List;

import com.hbo.eniso.mvc.dao.IMediaDao;
import com.hbo.eniso.mvc.entities.Media;
import com.hbo.eniso.mvc.services.IMediaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class MediaServiceImpl implements IMediaService {

	@Autowired
	private IMediaDao dao;

	@Override
	public Media save(Media entity) {
		return dao.save(entity);
	}

	@Override
	public Media update(Media entity) {
		return dao.update(entity);
	}

	@Override
	public List<Media> selectAll() {
		return dao.selectAll();
	}

	@Override
	public Media getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	@Override
	public Media findOne(String paramName, Object paramValue) {
		return dao.findOne(paramName, paramValue);
	}

}
