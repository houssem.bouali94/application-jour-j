package com.hbo.eniso.mvc.services.impl;

import java.util.List;

import com.hbo.eniso.mvc.dao.IIndustrielDao;
import com.hbo.eniso.mvc.entities.Industriel;
import com.hbo.eniso.mvc.services.IIndustrielService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class IndustrielServiceImpl implements IIndustrielService {

	@Autowired
	private IIndustrielDao dao;

	@Override
	public Industriel save(Industriel entity) {
		return dao.save(entity);
	}

	@Override
	public Industriel update(Industriel entity) {
		return dao.update(entity);
	}

	@Override
	public List<Industriel> selectAll() {
		return dao.selectAll();
	}

	@Override
	public Industriel getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	@Override
	public Industriel findOne(String paramName, Object paramValue) {
		return dao.findOne(paramName, paramValue);
	}

}
