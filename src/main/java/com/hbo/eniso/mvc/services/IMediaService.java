package com.hbo.eniso.mvc.services;

import java.util.List;

import com.hbo.eniso.mvc.entities.Media;

public interface IMediaService {
	
	public Media save(Media entity);

	public Media update(Media entity);

	public List<Media> selectAll();

	public Media getById(Long id);

	public void remove(Long id);

	public Media findOne(String paramName, Object paramValue);
}
