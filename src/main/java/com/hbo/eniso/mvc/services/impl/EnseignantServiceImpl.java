package com.hbo.eniso.mvc.services.impl;

import java.util.List;

import com.hbo.eniso.mvc.dao.IEnseignantDao;
import com.hbo.eniso.mvc.entities.Enseignant;
import com.hbo.eniso.mvc.services.IEnseignantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class EnseignantServiceImpl implements IEnseignantService {

	@Autowired
	private IEnseignantDao dao;

	@Override
	public Enseignant save(Enseignant entity) {
		return dao.save(entity);
	}

	@Override
	public Enseignant update(Enseignant entity) {
		return dao.update(entity);
	}

	@Override
	public List<Enseignant> selectAll() {
		return dao.selectAll();
	}

	@Override
	public Enseignant getById(Long id) {
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);
	}

	@Override
	public Enseignant findOne(String paramName, Object paramValue) {
		return dao.findOne(paramName, paramValue);
	}

}
