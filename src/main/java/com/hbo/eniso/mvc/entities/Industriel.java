package com.hbo.eniso.mvc.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Data
@Entity
public class Industriel implements Serializable {

    @Id
    @GeneratedValue
    @Column(nullable = false, unique = true)
    private Long idIndustriel;
    private String nom;
    private String prenom;
    private String poste;
    private String entreprise;
    private Long numeroTelephone;
    private String email;

}
