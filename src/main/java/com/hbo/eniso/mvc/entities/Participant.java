package com.hbo.eniso.mvc.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Data
@Entity
public class Participant implements Serializable {

    @Id
    @GeneratedValue
    @Column(nullable = false, unique = true)
    private Long idParticipant;
    private String nom;
    private String prenom;
    private String statut; // etudiant ou autre
    private String etablissement;
    private String email;

}
