package com.hbo.eniso.mvc.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Data
@Entity
public class Enseignant implements Serializable {

    @Id
    @GeneratedValue
    @Column(nullable = false, unique = true)
    private Long idProfessor;
    private String nom;
    private String prenom;
    private String grade;

}
