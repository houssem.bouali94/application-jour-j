package com.hbo.eniso.mvc.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Data
@Entity
public class Media implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    @Column(nullable = false, unique = true)
    private Long idMedia;
    private String nom;
    private String prenom;
    private String statut;
    private String chaine;
    private Long numTelephone;
    private String email;

}
