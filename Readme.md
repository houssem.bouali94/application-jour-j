## Application Jour J

### About
This application was developed in the name of the 7th edition of the forum (FCEE) held in National School of Engineers of Sousse (ENISO). It ensures the guests' registration and badges printing.

### Technologies
The following table illustrates the technologies that were user to develop the application at first time,
and the upgraded versions. 

| Technology       | base version  | upgraded version |
|------------------|---------------|------------------|
| Java             | 1.6           | 11               |
| Spring MVC       | 3.1.1.RELEASE | 5.1.0.RELEASE    |
| Hibernate        | 3.6.0.Final   | 5.3.5.Final      |
| MySQL            | 5.1.47        | 8.0.17           |
| Servlet          | 2.5           | 3.1.0            |
| JSP              | 2.1           | 2.3.1            |
| Jstl             | 1.2           | 1.2.1            |
| iTextPdf         | 5.4.0         | -                |
| Host environment | tomcat 7      | tomcat 9         |

### Migration
At first, the application was based on xml configuration files (application-context, servet-context,
persistence-context, spring-beans, etc...) and it got migrated to a java based based configuration.

